'use strict'

class FunctionState {
    static state(fn, states, defaultState) {
        const STATEVAR = '___state'
        const STATELIST = `${STATEVAR}_states`
        const STATEWATCHES = `${STATEVAR}_watches`

        function triggerStateWatch(fromState, toState, fn) {
            fn[STATEWATCHES].forEach(function (watch) {
                if ((watch.fromState == fromState || watch.fromState == '*') && (watch.toState == toState || watch.toState == '*')) {
                    watch.callback(fromState, toState, fn)
                }
            })
        }

        if (!Array.isArray(states)) states = [states.toString()]
            ; (function (fn, states, defaultState) {
                states.forEach(function (state) {
                    if (states.length > 1) {
                        Object.defineProperty(fn, `is${state.charAt(0).toUpperCase() + state.slice(1)}`, {
                            get: function () { return fn[STATEVAR] == state },
                            set: function (value) {
                                fn.state = !!value ? state : fn.state
                            }
                        })
                    } else {
                        Object.defineProperty(fn, `is${state.charAt(0).toUpperCase() + state.slice(1)}`, {
                            get: function () { return !!fn[`___${state}`] },
                            set: function (value) { fn[`___${state}`] = !!value }
                        })
                    }
                })
                if (states.length > 1) {
                    fn[STATELIST] = states
                    Object.defineProperty(fn, 'state', {
                        get: function () { return fn[STATEVAR] },
                        set: function (value) {
                            if (states.indexOf(value) == -1) throw new Error('Illegal State')
                            let oldVal = fn[STATEVAR]
                            fn[STATEVAR] = value
                            triggerStateWatch(oldVal, value, fn)
                        }
                    })

                    Object.defineProperty(fn, 'stateList', {
                        get: function () { return fn[STATELIST] }
                    })

                    fn[STATEWATCHES] = []
                    fn.state = defaultState

                    console.log(STATEWATCHES, fn[STATEWATCHES])

                    fn.stateWatch = function (fromState, toState, callback) {
                        fn[STATEWATCHES].push({ fromState, toState, callback })
                    }
                }
            })(fn, states, defaultState)
        return fn
    }

    static busyState(fn) {
        return FunctionState.state(fn, 'busy')
    }
}

module.exports = FunctionState
